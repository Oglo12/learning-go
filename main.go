package main

import (
	"fmt"
	"strings"
	"sort"
	"math"
)

func main() {
	fmt.Println("Hello, world!")

	testFunction() // In: other_file.go (No import statement or anything like that needed...)

	names := []string{"Jackson", "Ezra", "Jason", "Jeff", "Frank", "Farkus"}

	cycleNames(names, sayHello)

	exampleVariables()
	exampleFmt()
	exampleArraysAndSlices()
	exampleOtherStdStuff()
	exampleLoops()
	exampleConditionals()

	radius := 15.0

	fmt.Printf("Area for circle with radius: %0.2f is: %0.2f\n", radius, circleArea(radius))

	cycleNames(names, sayGoodbye)
}

func circleArea(radius float64) float64 {
	return math.Pi * radius * radius
}

func cycleNames(names []string, code func(string)) {
	for _, i := range names {
		code(i)
	}
}

func sayHello(name string) {
	fmt.Printf("Hello, %v!\n", name)
}

func sayGoodbye(name string) {
	fmt.Printf("Goodbye, %v!\n", name)
}

func exampleConditionals() {
	x := 45
	y := 20
	z := 52

	if x < 30 {
		fmt.Println("X is less than 30!")
	} else if x > 50 {
		fmt.Println("X is more than 50!")
	} else {
		fmt.Printf("X is %v.\n", x)
	}

	if x == 45 && y == 20 {
		fmt.Println("X is 45, and Y is 20.")
	}

	if z > x {
		fmt.Println("Z is more than X.")
	}
}

func exampleLoops() {
	x := 0
	for x <= 5 {
		fmt.Println("->", x)
		x++
	}

	for i := 0; i <= 5; i++ {
		fmt.Println("->", i)
	}

	var words []string = []string{"the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"}

	for i := 0; i < len(words); i++ {
		fmt.Println("->", words[i])
	}

	for i, v := range words {
		fmt.Printf("-> %v, %v\n", i, v)
	}

	for _, v := range words {
		fmt.Printf("-> %v\n", v)
	}
}

func exampleOtherStdStuff() {
	// String manipulation.
	var myString string = "The quick brown fox jumps over the lazy dog."

	fmt.Printf("String: '%v'\n", myString)
	fmt.Printf("String Contains 'quick': '%v'\n", strings.Contains(myString, "quick"))
	fmt.Printf("String Replace 'quick' w/ 'fast': '%v'\n", strings.ReplaceAll(myString, "quick", "fast"))

	// Sorting.
	var myNumbers []int = []int{45, 42, 57, 64, 100, 2, 9}
	fmt.Printf("My Numbers: %v\n", myNumbers)
	sort.Ints(myNumbers) // sort.Ints() actually changes the array you pass into it.
	fmt.Printf("My Numbers (Sorted): %v\n", myNumbers)

	var myStrings []string = []string{"the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"}
	fmt.Printf("My Strings: %v\n", myStrings)
	sort.Strings(myStrings) // sort.Strings() also changes the array you pass into it.
	fmt.Printf("My Strings (Sorted): %v\n", myStrings)
}

func exampleArraysAndSlices() {
	// Arrays.
	var numbers [5]int = [5]int{5, 2, 9, 6, 4}
	words := [5]string{"the", "fast", "brown", "fox", "jumps"}

	words[1] = "quick"

	fmt.Printf("Numbers Items: %v | Numbers Length: %v\n", numbers, len(numbers))
	fmt.Printf("Words Items: %v | Words Length: %v\n", words, len(words))

	// Slices. - Slices are basically the same thing as vectors.
	var stuff []int = []int{6, 2}

	stuff = append(stuff, 85)
	stuff = append(stuff, 62)

	fmt.Printf("Stuff Items: %v | Stuff Length: %v\n", stuff, len(stuff))

	start := 1
	end := 3

	stuffRange1 := stuff[start:end] // This essentially creates a completely new value. It does not reference the 'stuff' slice.
	stuffRange2 := stuff[end:]      // So you could do something like this: 'stuffRange1 = append(stuffRange1, 45)'
	stuffRange3 := stuff[:start]

	fmt.Printf("Stuff[%v..%v] Items: %v | Stuff[%v..%v] Length: %v\n", start, end, stuffRange1, start, end, len(stuffRange1))
	fmt.Printf("Stuff[%v..] Items: %v | Stuff[%v..] Length: %v\n", end, stuffRange2, end, len(stuffRange2))
	fmt.Printf("Stuff[..%v] Items: %v | Stuff[..%v] Length: %v\n", start, stuffRange3, start, len(stuffRange3))
}

func exampleFmt() {
	a := "A"
	b := "B"
	c := "C"
	x := 8
	y := 9
	z := 10

	fmt.Printf("A: %v %q %T\n", a, a, a)
	fmt.Printf("B: %v %q %T\n", b, b, b)
	fmt.Printf("C: %v %q %T\n", c, c, c)
	fmt.Printf("X: %v %q %T\n", x, x, x)
	fmt.Printf("Y: %v %q %T\n", y, y, y)
	fmt.Printf("Z: %v %q %T\n", z, z, z)

	f := 6.58

	fmt.Printf("F: %f\n", f)
	fmt.Printf("F: %0.2f\n", f)
	fmt.Printf("F: %0.1f\n", f)

	name := "Jackson"

	my_name_is := fmt.Sprintf("My name is %v!", name)

	fmt.Println(my_name_is)
}

func exampleVariables() {
	var first_name string = "Jason"
	last_name := "Novak"

	first_name = "Jackson"

	fmt.Println("Name:", first_name, last_name)

	// Numbers:
	//   Bases:
	//     int
	//     uint
	//     float
	//   Bits:
	//     8
	//     16
	//     32
	//     64
	var x uint8 = 5
	var y uint8 = 7
	z := x + y

	fmt.Println(x, "+", y, "=", z)

	var fx float64 = 6.4
	var fy float32 = 3.2
	var fz float64 = fx + float64(fy)

	fmt.Println(fx, "+", fy, "=", fz)
}
