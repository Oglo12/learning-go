package main

import (
	"fmt"
)

func testFunction() {
	fmt.Println("A public function that is usable in other files.")
}
